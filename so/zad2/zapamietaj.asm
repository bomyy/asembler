section	.text

global pix

; void pix(uint32_t *ppi, uint64_t *pidx, uint64_t max)
; rdi = *ppiw
; rsi = *pidx
; rdx = max
pix:
	mov	r14, rdx

loop1:
	; wynik trzymamy w r9
	; pojedyńcze wyniki liczymy na r10
	mov	r8, 1
	lock xadd [rsi], r8 ;zaladuj indeks do r8

	cmp	r8, r14
	jge	loop1_end ;jesli jestesmy poza maksem to konczymy

	xor	r9, r9
	; 8 * k + 1, inf_sum
	mov	rax, r8
	mov	r10, 8
	mul	r10
	add	rax, 1
	call	inf_sum
	mov	r10, 4
	mul	r10
	add	r9, rax
	
	; 8 * k + 4, inf_sum
        mov     rax, r8
	mov	r10, 8
        mul     r10
        add     rax, 4
        call    inf_sum
	mov	r10, 2
        mul     r10
        sub     r9, rax

        ; 8 * k + 5, inf_sum
        mov     rax, r8
        mov     r10, 8
        mul     r10
        add     rax, 5
        call    inf_sum
        sub     r9, rax

        ; 8 * k + 6, inf_sum
        mov     rax, r8
        mov     r10, 8
        mul     r10
        add     rax, 6
        call    inf_sum
        sub	r9, rax

	; 8 * k + 1, dys_sum
        mov     rax, r8
        mov     r15, 8
        mul     r15
        add     rax, 1
        mov     r15, rax
	call	dys_sum
	mov	r10, 4
	mul	r10
	add	r9, rax

        ; 8 * k + 4, dys_sum
        mov     rax, r8
        mov     r15, 8
        mul     r15
        add     rax, 4
        mov     r15, rax
        call    dys_sum
        mov     r10, 2
        mul     r10
        sub     r9, rax

        ; 8 * k + 5, dys_sum
        mov     rax, r8
        mov     r15, 8
        mul     r15
        add     rax, 5
        mov     r15, rax
        call    dys_sum
        sub     r9, rax
	
        ; 8 * k + 6, dys_sum
        mov     rax, r8
        mov     r15, 8
        mul     r15
        add     rax, 6
        mov     r15, rax
        call    dys_sum
        sub     r9, rax

	shr	r9, 32
	mov	[rdi + r8], r9d

	jmp	loop1
loop1_end:
	ret

; rax = 8 * k + cons
; rax = zwracany wynik
; zmieniane rejestry: rax, r10, r11, r12(licznik), r13
; liczymy od n + 1
inf_sum:
	xor	r10, r10
	mov	r11, rax
	xor	r12, r12
	not	r12
	; r12 = 2^64, r11 = 8 * k + cons, r10 = 0

loop2:
	cmp	r12, 0
	je	loop2_end

	mov	rax, r12 ; licznik -> rax
	xor	rdx, rdx
	mov	r13, 16
	div	r13 ; rax / 16
	mov	r12, rax ; uaktualnia licznik
	xor	rdx, rdx
        add     r11, 8 ; aktualnizuje mianownik
	div	r11 ; liczy licznik / mianownik
	add	r10, rax ; dodaje do wyniku

	jmp	loop2

loop2_end:
	mov	rax, r10
	ret

; r15 = 8 * k + cons
; r8 = n
; zmieniane rejestry: rax, rdx, r10, r11, r12
; rax = zwracany wynik
; nie ruszac: r11(indeks petli), r10(wynik)
dys_sum:
	xor	rax, rax
	ret
	xor	r10, r10
	mov	r11, r8

loop3:

	mov	r13, r8 ; r13 = b w a^b mod c
	sub	r13, r11

	push	r8 
	push	r9
	
	mov	r9, 16
	mov	r8, 1
; r8 = wynik, r9 = a, r13 = b
loop4:
	xor	rdx, rdx
	mov	rax, r13
	mov	r13, 2
	div	r13
	mov	r13, rax
	; b /= 2

	cmp	rdx, 0
	je	if_end1

	; jesli b % 2 == 1
	xor	rdx, rdx
	mov	rax, r9
	mul	r8
	div	r15
	mov	r8, rdx

if_end1:

	; r9 = r9^2 mod r15
	mov	rax, r9
	mul	rax
	xor	rdx, rdx
	div	r15
	mov	r9, rdx

	cmp	r13, 0
	jne	loop4
loop4_end:

	mov	rdx, r8
	xor	rax, rax
	div	r15
	add	r10, rdx

	pop	r9
	pop	r8

	cmp	r11, 0
	je	loop3_end
	sub	r11, 1
	sub	r15, 8

	jmp	loop3
loop3_end:

	mov	rax, r10
	ret
