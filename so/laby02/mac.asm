global mac

mac:
    ; rdi
    ; rsi
    ; r9
    mov r9, rdx
    mov rax, [rsi]
    mov r10, [r9]
    mul r10
    add rax, [rdi]
    
    adc rdx, [rdi + 8]
    ; [rsi + 8] i [r9 + 8]
    
    mov r10, [rsi + 8]
    mov rdi, [r9]
    imul r10, rdi
    add rdx, r10
    
    
    mov r10, [rsi]
    mov rdi, [r9 + 8]
    imul r10, rdi
    add rdx, r10
    ret
