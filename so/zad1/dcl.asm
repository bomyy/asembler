sys_read equ 3
sys_write equ 4
stdin equ 0
stdout equ 1
buff_len equ 6000
Plen equ 42

global _start

section .bss

buff:	resb buff_len
L:	resb Plen
R:      resb Plen
T:      resb Plen
Linv:   resb Plen
Rinv:   resb Plen
Tinv:   resb Plen
l:	resb 1
r:	resb 1

section .text

; urzywane: rdi, rsi, rax, r14
; parametry: rdi - char, rsi - *char, rax - char
; oblicza Q_(rdi)^-1 (rsi) Q_(rdi)
; początkowy znak jak i wynik znajduje się w rax
_cypher_step:
	call	_shift_perm

	call	_apply_perm

	mov	r14, 42
	sub	r14, rdi
	mov	rdi, r14
	call	_shift_perm
	ret

; urzywane: rax, rsi
; wykonuje rax = rsi[rax]
_apply_perm:
	mov	al, [rsi + rax]
	ret

; urzywane: rax, rdi
; przesuwa rax o rdi liczb w prawo, modulo 42
_shift_perm:
	add	rax, rdi
	add	rax, Plen

loop5:
	cmp	rax, Plen
	jl	end_loop5
	sub	rax, Plen
	jmp	loop5

end_loop5:
	ret

; urzywa: r13, r12
; oblicza nowe wartości l oraz r
_next_position:
	mov	r13b, [r]
	mov	r12b, [l]
	add	r13b, 1

	cmp	r13b, Plen
	jne	next_pos_ifs
	mov	r13b, 0

next_pos_ifs:

	cmp	r13b, 27
	je	add_l
        cmp     r13b, 35
        je      add_l
        cmp     r13b, 33
        je      add_l
	jmp	end_next_pos

add_l:
	add	r12b, 1
	cmp	r12b, Plen
	jne	end_next_pos
	mov	r12b, 0

end_next_pos:
	mov	[r], r13b
	mov	[l], r12b
	ret

; urzywa: rax
; sprawdza czy rax >= 49 i rax <= 90
_check_char:
        cmp	al, 49
        jl	_end
        cmp	al, 90
        jg	_end
	ret

; urzywa: r11, r10, rax, rdi, rsi, rdx
; rdi - oryginalna permutacja
; rsi - kopia permutacji
; rdx - permutacja ^ -1
_normalize:
	xor	r10, r10
loop1:
	xor	rax, rax
	mov	al, [rdi]

	cmp	al, 0
	je	end_loop1

	call	_check_char

	; przeskanuj znaki o 49 w lewo
	sub	al, 49

	mov	r11b, [rdx + rax]
	cmp	r11b, 0
	jnz	_end

	mov	[rsi + r10], al
	mov	[rdx + rax], r10b

	add	rdi, 1
	add	r10, 1
	jmp	loop1

end_loop1:
	cmp	r10, Plen
	jnz	_end
	ret

; urzywa: rax, rdi, rsi, r10, r11
; sprawdza czy T == T^-1, oraz czy T nie zawiera punktów stałych
; rdi - T
; rsi - T^-1
_is_equal:
	xor	rax, rax
loop2:
	cmp	rax, Plen
	je	end_loop2

	mov	r10b, [rdi]
	mov	r11b, [rsi]

	; if T[i] == T[i] exit(1)
	cmp	r10b, al
	je	_end

	; if not equal exit(1)
	cmp	r10b, r11b
	jne	_end

	add	rdi, 1
	add	rsi, 1
	add	rax, 1
	jmp	loop2
end_loop2:
	ret

_start:
	; sprawdza liczbę parametrów
	mov	rax, [rsp]
	cmp	rax, 5
	jne	_end

	; _normalize L i Linv
	mov	rdi, [rsp + 8 * 2]
	mov	rsi, L
	mov	rdx, Linv
	call	_normalize	

        ; _normalize R i Rinv
        mov	rdi, [rsp + 8 * 3]
        mov	rsi, R
        mov	rdx, Rinv
        call	_normalize

        ; _normalize T i Tinv
        mov	rdi, [rsp + 8 * 4]
        mov	rsi, T
        mov	rdx, Tinv
        call	_normalize

	mov	rdi, T
	mov	rsi, Tinv
	call	_is_equal

	; pobierz, sprawdz i zapisz wartość l	
	xor	rax, rax
	mov	rdi, [rsp + 8 * 5]
	mov	al, [rdi]
	call	_check_char
	sub	al, 49
	mov	[l], al

        ; pobierz, sprawdz i zapisz wartość r
        xor     rax, rax
        mov     rdi, [rsp + 8 * 5]
        mov     al, [rdi + 1]
        call	_check_char
        sub	al, 49
	mov	[r], al

	; sprawdz czy 4 parametr nie jest za długi
	xor 	rax, rax
        mov     rdi, [rsp + 8 * 5]
        mov     al, [rdi + 2]
	cmp	rax, 0
	jne	_end

loop3:	
	; wczytywanie
	mov	rax, sys_read
	mov	rbx, stdin
	mov	rcx, buff
	mov	rdx, buff_len
	int 0x80

        mov     r10, buff ; wskaźnik na bufor
        mov     r11, rax ; długość bufora
	xor	r15, r15

	cmp	r11, 0
	je	end_loop3

loop4:
	cmp	r11, r15
	je	end_loop4

	xor	rax, rax
	mov	al, [r10]

	call	_check_char
	sub	al, 49

	; liczy nowe l i r
	call	_next_position

	; początek szyfrowania
	xor	rdi, rdi
	mov	dil, [r]
	mov	rsi, R
	call	_cypher_step

        xor     rdi, rdi
        mov     dil, [l]
        mov     rsi, L
        call    _cypher_step

	mov	rsi, T
	call	_apply_perm

        xor     rdi, rdi
        mov     dil, [l]
        mov     rsi, Linv
        call    _cypher_step

        xor     rdi, rdi
        mov     dil, [r]
        mov     rsi, Rinv
        call    _cypher_step
	; koniec szyfrowanie

	add	al, 49
	mov	[r10], al
	add	r10, 1
	add	r15, 1
	jmp	loop4

end_loop4:

	; wypisz rufor
	mov	rax, sys_write
	mov	rbx, stdout
	mov	rcx, buff
	mov	rdx, r15
	int	0x80

        jmp      loop3

end_loop3:
	
	mov	rax, 1
	mov	rbx, 0
	int	0x80

_end:
	mov	rax, 1
	mov	rbx, 1
	int	0x80
