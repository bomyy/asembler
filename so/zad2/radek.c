#include <stdio.h>
#include <stdlib.h>
#include "pix.h"
const unsigned long long nax=1000;
#define ll uint64_t
#define ii uint32_t

void pixtime(uint64_t clock_tick)
{
    fprintf(stderr, "%lu\n", clock_tick);
}

int main()
{
    ii tab[nax];
    ll counter=0;
    ll limit=nax;
    for (int i=0; i<nax; i++)
        tab[i]=0;
    pix(tab, &counter, limit);
    int i;
    for (i=0; i<nax*32; i++)
        printf("%d", (int)((tab[i/32]>>(32-1-i%32))&1));
    printf("\n");
}
