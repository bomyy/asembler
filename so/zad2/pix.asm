extern pixtime
global pix

; void pix(uint32_t *ppi, uint64_t *pidx, uint64_t max)
pix:
	; zmieniane są rejestry r12 - r15, więc zapisywane są na stosie
	push	r12
	push	r13
	push	r14
	push	r15

	mov	r14, rdx
	call	call_pixtime

loop1:
	mov	r8, 1
	lock \
	xadd	[rsi], r8 ; pobierz pidx i zwiększ go o 1

	cmp	r8, r14
	jge	loop1_end ;jesli pidx >= max przerwij petle

	shl	r8, 3 ; pomnoz pidx przez 8, czyli dostajemy od
		      ; ktorej pozycji chcemy policzyc

	xor	r9, r9

	; liczenie sumy od n + 1 do inf, dla mianownika 8 * k + 1
	mov	rax, r8
	mov	r10, 8
	mul	r10
	add	rax, 1 ; liczenie mianownika i zapisywanie go w rax
	call	inf_sum
	mov	r10, 4
	mul	r10
	add	r9, rax ; do wyniku zapisujemy 4 * policzona_suma
	
        ; liczenie sumy od n + 1 do inf, dla mianownika 8 * k + 4
        mov     rax, r8
	mov	r10, 8
        mul     r10
        add     rax, 4
        call    inf_sum
	mov	r10, 2
        mul     r10
        sub     r9, rax ; od wyniku odejmujemy 2 * policzona_suma

        ; liczenie sumy od n + 1 do inf, dla mianownika 8 * k + 5
        mov     rax, r8
        mov     r10, 8
        mul     r10
        add     rax, 5
        call    inf_sum
        sub     r9, rax ; od wyniku odejmujemy policzona_suma

        ; liczenie sumy od n + 1 do inf, dla mianownika 8 * k + 6
        mov     rax, r8
        mov     r10, 8
        mul     r10
        add     rax, 6
        call    inf_sum
        sub	r9, rax ; od wyniku odejmujemy policzona_suma

        ; liczenie sumy od 0 do n, dla mianownika 8 * k + 1
        mov     rax, r8
        mov     r15, 8
        mul     r15
        add     rax, 1
        mov     r15, rax
	call	dys_sum
	mov	r10, 4
	mul	r10
	add	r9, rax ; do wyniku dodajemy 4 * policzona_suma

         ; liczenie sumy od 0 do n, dla mianownika 8 * k + 4
        mov     rax, r8
        mov     r15, 8
        mul     r15
        add     rax, 4
        mov     r15, rax
        call    dys_sum
        mov     r10, 2
        mul     r10
        sub     r9, rax ; od wyniku odejmujemy 2 * policzona_suma

        ; liczenie sumy od 0 do n, dla mianownika 8 * k + 5
        mov     rax, r8
        mov     r15, 8
        mul     r15
        add     rax, 5
        mov     r15, rax
        call    dys_sum
        sub     r9, rax ; od wyniku odejmujemy policzona_suma
	
        ; liczenie sumy od 0 do n, dla mianownika 8 * k + 6
        mov     rax, r8
        mov     r15, 8
        mul     r15
        add     rax, 6
        mov     r15, rax
        call    dys_sum
        sub     r9, rax ; od wyniku odejmujemy 2 * policzona_suma

	shr	r9, 32
	shr	r8, 3
	mov	[rdi + r8 * 4], r9d

	jmp	loop1
loop1_end:
	call	call_pixtime
	pop	r15
	pop	r14
	pop	r13
	pop	r12
	ret

; rax - mianownik do sumy
; r11 - mianownik do sumy
; r12 - licznik do sumy
; rax - zwracany wynik
; zmieniane rejestry: rax, r10, r11, r12, r13
inf_sum:
	xor	r10, r10
	mov	r11, rax
	xor	r12, r12
	not	r12

loop2:
	cmp	r12, 0 ; jesli licznik jest juz niedokladny
	je	loop2_end ; przerwij petle

	mov	rax, r12
	xor	rdx, rdx
	mov	r13, 16
	div	r13 ; dzieli licznik przez 16
	mov	r12, rax ; uaktualnia licznik do nastepnego obrotu
	xor	rdx, rdx
        add     r11, 8 ; aktualnizuje mianownik
	div	r11 ; liczy licznik / mianownik
	add	r10, rax ; dodaje licznik / mianownik do wyniku

	jmp	loop2

loop2_end:
	mov	rax, r10
	ret

; zmieniane rejestry: rax, rdx, r10, r11, r12, r13, r15
dys_sum:
	;xor	rax, rax
	;ret
	xor	r10, r10
	mov	r11, r8

loop3:

	mov	r13, r8
	sub	r13, r11

	push	r8 
	push	r9

	mov	r8, 0
	cmp	r15, 1
	je	loop4_end
	
	mov	r9, 16
	mov	r8, 1
; r8 = wynik, r9 = a, r13 = b
loop4:
	xor	rdx, rdx
	mov	rax, r13
	mov	r13, 2
	div	r13
	mov	r13, rax
	; b /= 2

	cmp	rdx, 0
	je	if_end1

	; jesli b % 2 == 1
	xor	rdx, rdx
	mov	rax, r9
	mul	r8
	div	r15
	mov	r8, rdx

if_end1:

	; r9 = r9^2 mod r15
	mov	rax, r9
	mul	rax
	xor	rdx, rdx
	div	r15
	mov	r9, rdx

	cmp	r13, 0
	jne	loop4
loop4_end:

	mov	rdx, r8
	xor	rax, rax
	div	r15
	add	r10, rax

	pop	r9
	pop	r8

	cmp	r11, 0
	je	loop3_end
	sub	r11, 1
	sub	r15, 8

	jmp	loop3
loop3_end:

	mov	rax, r10
	ret

; wywoluje funkcje pixtime, jednoczesnie zachowujac
; wartosci rejestrow
call_pixtime:
	mov	r15, rdi
        mov     r13, rsi

	rdtsc

	mov	rdi, rdx
	shl	rdi, 32
	add	rdi, rax

	call	pixtime

	mov	rdi, r15
        mov     rsi, r13
	ret

